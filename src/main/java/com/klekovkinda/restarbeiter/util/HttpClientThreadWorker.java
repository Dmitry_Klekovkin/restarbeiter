package com.klekovkinda.restarbeiter.util;

import com.klekovkinda.restarbeiter.beans.BddPicoContainer;
import com.klekovkinda.restarbeiter.beans.HTTPClientBean;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


public class HttpClientThreadWorker implements Runnable {
    private Map<HTTPClientBean.RequestFields, String> requestDescriptor;
    private CountDownLatch startSignal;
    private CountDownLatch doneSignal;
    private Map<String, Object> mainCache;

    public HttpClientThreadWorker(Map<HTTPClientBean.RequestFields, String> requestDescriptor, CountDownLatch startSignal, CountDownLatch doneSignal, Map<String, Object> mainCache) {
        this.requestDescriptor = requestDescriptor;
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
        this.mainCache = mainCache;
    }

    public void run() {
        map().putAll(this.mainCache);
        try {
            startSignal.await();
            httpClientBean().doRequest(requestDescriptor);
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        doneSignal.countDown();
    }

    private HTTPClientBean httpClientBean() {
        return BddPicoContainer.instance().getComponent(HTTPClientBean.class);
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }

}
