package com.klekovkinda.restarbeiter.util;

import com.klekovkinda.restarbeiter.beans.AbstractEntityUtil;
import com.klekovkinda.restarbeiter.beans.MapUtilBean;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.util.Map;

public enum TypeReferenceMapper {
    STRING("String",
            new TypeReference<String>() {
            },
            null),
    LIST__STRING("List<String>",
            new TypeReference<List<String>>() {
            },
            null),
    LIST__OBJECT("List<Object>",
            new TypeReference<List<Map>>() {
            }, MapUtilBean.class),
    OBJECT("Object",
            new TypeReference<Map>() {
            }, MapUtilBean.class);


    private final TypeReference typeReference;
    private final String type;
    private final Class<? extends AbstractEntityUtil> utilClass;

    TypeReferenceMapper(String type, TypeReference typeReference, Class<? extends AbstractEntityUtil> utilClass) {
        this.type = type;
        this.typeReference = typeReference;
        this.utilClass = utilClass;
    }

    public Class<? extends AbstractEntityUtil> getUtilClass() {
        return utilClass;
    }

    public TypeReference getTypeReference() {
        return typeReference;
    }

    public String toString() {
        return type;
    }

    public static TypeReferenceMapper getByType(String type) {
        for (TypeReferenceMapper typeReferenceMapper : values()) {
            if (typeReferenceMapper.toString().equals(type)) {
                return typeReferenceMapper;
            }
        }
        return OBJECT;
    }
}
