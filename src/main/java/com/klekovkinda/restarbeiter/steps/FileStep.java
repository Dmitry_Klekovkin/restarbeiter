package com.klekovkinda.restarbeiter.steps;

import com.klekovkinda.restarbeiter.beans.BddPicoContainer;
import com.klekovkinda.restarbeiter.beans.FileWorkerBean;
import cucumber.api.java.en.Given;

import java.io.IOException;
import java.util.HashMap;

public class FileStep {

    @Given("^Load file (.*) as string (.*)")
    public void loadFile(String filePath, String key) throws IOException {
        map().put(key, fileWorkerBean().readFile(filePath));
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }

    private FileWorkerBean fileWorkerBean() {
        return BddPicoContainer.instance().getComponent(FileWorkerBean.class);
    }

}
