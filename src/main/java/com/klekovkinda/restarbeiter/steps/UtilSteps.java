package com.klekovkinda.restarbeiter.steps;

import com.klekovkinda.restarbeiter.beans.BddPicoContainer;
import com.klekovkinda.restarbeiter.beans.UtilBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import gherkin.formatter.model.DataTableRow;
import org.junit.Assert;

import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class UtilSteps {

    @Then("^Variable mapping$")
    public void variableMapping(DataTable dataTable) throws Exception {
        for (int i = 0; i < dataTable.getGherkinRows().size(); i++) {
            DataTableRow row = dataTable.getGherkinRows().get(i);
            if (row.getCells().size() == 2) {
                String key = row.getCells().get(0);
                String value = utilBean().replace(row.getCells().get(1));
                if (!map().containsKey(key)) {
                    map().put(key, value);
                }
            }
        }
    }

    @Then("^Encode (.*) to base 64 and put to (.*)$")
    public void base64putTo(String value, String id) throws JsonProcessingException {
        String base64EncodedString = Base64.getEncoder().encodeToString(utilBean().replace(value).getBytes());
        map().put(id, base64EncodedString);
    }

    @Then("^Object from (.*) equals to (.*)$")
    public void compareValues(String expectedValueId, String actualValueId) throws Throwable {
        Assert.assertEquals(map().get(expectedValueId), map().get(actualValueId));
    }

    @Then("^Collection from (.*) contains (.*)$")
    public void setContains(String setId, String actualValueId) throws Throwable {
        Assert.assertTrue(((Collection) map().get(setId)).contains(map().get(actualValueId)));
    }

    @Then("^value (.*) equals to (.*)$")
    public void compareStringValues(String expectedValue, String actualValue) throws Throwable {
        Assert.assertEquals(utilBean().replace(expectedValue), utilBean().replace(actualValue));
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }

    private UtilBean utilBean() {
        return BddPicoContainer.instance().getComponent(UtilBean.class);
    }

}
