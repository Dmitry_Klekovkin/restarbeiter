package com.klekovkinda.restarbeiter.steps;

import com.klekovkinda.restarbeiter.beans.BddPicoContainer;
import com.klekovkinda.restarbeiter.beans.HTTPClientBean;
import com.klekovkinda.restarbeiter.beans.UtilBean;
import com.klekovkinda.restarbeiter.util.HttpClientThreadWorker;
import com.klekovkinda.restarbeiter.util.TypeReferenceMapper;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.Assert;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class HttpSteps {

    @Given("http request")
    public void httpRequest(DataTable dataTable) throws UnirestException, IOException {
        httpClientBean().doRequest(utilBean().convertGherkinRowsToRequestMap(dataTable));
    }

    @Given("skip ssl validation")
    public void skipSSLValidation() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpClientBuilder b = HttpClientBuilder.create();

        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                return true;
            }
        }).build();
        b.setSslcontext(sslContext);

        HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, (X509HostnameVerifier) hostnameVerifier);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();

        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        b.setConnectionManager(connMgr);
        httpClientBean().setHttpClient(b.build());
    }

    @Then("^HttpResponse.status should be: (.+)$")
    public void httpStatusCodeShouldBe(int statusCode) {
        Assert.assertEquals(statusCode, httpClientBean().getLastHttpResponseStatus());
    }

    @Then("^Put httpResponse.body and put to (.*)$")
    public void httpResponseBody(String key) {
        map().put(key, httpClientBean().getLastResponceStringFromHttpResponce());
    }


    @Then("^read (.*) form the httpResponse and put to (.*)$")
    public void readEntityAndPutTo(String entityType, String id) throws IOException {
        TypeReferenceMapper mapper = TypeReferenceMapper.getByType(entityType);
        Object value = httpClientBean().buildLastResponseObjectFromHttpResponseString(mapper);
        if (mapper.getTypeReference().getType() instanceof ParameterizedType &&
                ((Class) (((ParameterizedType) mapper.getTypeReference().getType()).getRawType())).isAssignableFrom(List.class)) {
            BddPicoContainer.instance().getComponent(mapper.getUtilClass()).doIndexFieldsInList(id, (List) value);
        } else {
            BddPicoContainer.instance().getComponent(mapper.getUtilClass()).doIndexFields(id, value);
        }
    }

    private HTTPClientBean httpClientBean() {
        return BddPicoContainer.instance().getComponent(HTTPClientBean.class);
    }

    private UtilBean utilBean() {
        return BddPicoContainer.instance().getComponent(UtilBean.class);
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }

}
