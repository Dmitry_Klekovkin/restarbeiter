package com.klekovkinda.restarbeiter.beans;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.behaviors.ThreadCaching;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

public class BddPicoContainer extends DefaultPicoContainer {

    private static BddPicoContainer instance;

    public BddPicoContainer(ThreadCaching caching) {
        super(caching);
    }

    public static BddPicoContainer instance() {
        instance = instance == null ? new BddPicoContainer(new ThreadCaching()) : instance;
        return instance;
    }

    @Before
    public void setUp() throws UnknownHostException {
        BddPicoContainer.instance()
                .addComponent(HashMap.class)
                .addComponent(new ConstantsBean())
                .addComponent(HTTPClientBean.class)
                .addComponent(MapUtilBean.class)
                .addComponent(UtilBean.class)
                .addComponent(FileWorkerBean.class);
    }

    @After
    public void tearDown() throws ClassNotFoundException {
        List<DownAble> downAbles = instance().getComponents(DownAble.class);
        for (DownAble downAble : downAbles) {
            downAble.down();
        }
        instance = null;
    }
}
