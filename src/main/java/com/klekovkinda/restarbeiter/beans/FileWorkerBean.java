package com.klekovkinda.restarbeiter.beans;

import java.io.*;

public class FileWorkerBean {

    public String readFile(String filePath) throws IOException {
        Reader fileReader = new FileReader(filePath);
        BufferedReader bufReader = new BufferedReader(fileReader);
        StringBuilder stringBuilder = new StringBuilder();
        String line = bufReader.readLine();
        while (line != null) {
            stringBuilder.append(line);
            line = bufReader.readLine();
        }
        return utilBean().replace(stringBuilder.toString());
    }

    private UtilBean utilBean() {
        return BddPicoContainer.instance().getComponent(UtilBean.class);
    }
}
