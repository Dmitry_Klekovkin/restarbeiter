package com.klekovkinda.restarbeiter.beans;

import com.klekovkinda.restarbeiter.util.TypeReferenceMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.apache.http.client.HttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class HTTPClientBean {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private HttpResponse<String> httpResponseString;

    public void doRequest(Map<RequestFields, String> requestMap) throws UnirestException, IOException {
        HttpMethod method = HttpMethod.valueOf(requestMap.get(RequestFields.METHOD).toString());
        HttpRequest httpRequest = null;
        String uri = utilBean().replace(requestMap.get(RequestFields.URI).toString());
        String body = null;
        byte[] bodyBytes = null;
        File bodyFile = null;
        httpResponseString = null;
        switch (method) {
            case GET: {
                httpRequest = Unirest.get(uri);
                break;
            }
            case DELETE: {
                httpRequest = Unirest.delete(uri);
                break;
            }
            case PUT: {
                httpRequest = Unirest.put(uri);
                if (requestMap.get(RequestFields.BODY) != null) {
                    body = utilBean().replace(requestMap.get(RequestFields.BODY).toString());
                    ((HttpRequestWithBody) httpRequest).body(body);
                } else if (requestMap.get(RequestFields.BODY_FILE) != null) {
                    bodyFile = new File(requestMap.get(RequestFields.BODY_FILE));
                    bodyBytes = new byte[(int) bodyFile.length()];
                    FileInputStream fileInputStream = new FileInputStream(bodyFile);
                    fileInputStream.read(bodyBytes);
                    fileInputStream.close();
                    ((HttpRequestWithBody) httpRequest).body(bodyBytes);
                }
                break;
            }
            case POST: {
                httpRequest = Unirest.post(uri);
                if (requestMap.get(RequestFields.BODY) != null) {
                    body = utilBean().replace(requestMap.get(RequestFields.BODY).toString());
                    ((HttpRequestWithBody) httpRequest).body(body);
                } else if (requestMap.get(RequestFields.BODY_FILE) != null) {
                    bodyFile = new File(requestMap.get(RequestFields.BODY_FILE));
                    bodyBytes = new byte[(int) (bodyFile.length())];
                    FileInputStream fileInputStream = new FileInputStream(bodyFile);
                    fileInputStream.read(bodyBytes);
                    fileInputStream.close();
                    ((HttpRequestWithBody) httpRequest).body(bodyBytes);
                }
                break;
            }
            default: {
                throw new RuntimeException("Unsupported http method " + method);
            }
        }
        if (requestMap.get(RequestFields.HEADER) != null && !"".equals(requestMap.get(RequestFields.HEADER).toString().trim())) {
            String headerJson = requestMap.get(RequestFields.HEADER).toString();
            headerJson = utilBean().replace(headerJson);
            Map<String, String> headers = objectMapper.readValue(headerJson, new TypeReference<Map<String, String>>() {
            });
            Set<String> headerKeys = headers.keySet();
            for (String headerKey : headerKeys) {
                httpRequest.header(headerKey, headers.get(headerKey));
            }
        }
        System.out.println("=======\\/HTTP\\/========");
        if (requestMap.get(RequestFields.DESCRIPTION) != null) ;
        {
            System.out.println("Description: " + requestMap.get(RequestFields.DESCRIPTION));
        }
        System.out.println("Request " +
                "\n\tmethod\t" + httpRequest.getHttpMethod() +
                "\n\turi\t\t" + httpRequest.getUrl() +
                "\n\theader\t\t" + httpRequest.getHeaders() +
                (bodyBytes == null ?
                        "\n\tbody\t" + body :
                        "\n\tbodyFile\t" + bodyFile.getAbsolutePath() +
                                "\n\tbodyBytes.length\t" + bodyBytes.length));
        httpResponseString = httpRequest.asString();
        System.out.println("Response " +
                "\n\tstatus\t\t" + httpResponseString.getStatus() +
                "\n\tstatus text\t" + httpResponseString.getStatusText() +
                "\n\tbody\t\t" + httpResponseString.getBody() +
                "\n");
        System.out.println("========/\\HTTP/\\========");
    }

    public Object buildLastResponseObjectFromHttpResponseString(TypeReferenceMapper typeReferenceMapper) throws IOException {
        return objectMapper.readValue(httpResponseString.getBody(), typeReferenceMapper.getTypeReference());
    }

    public String getLastResponceStringFromHttpResponce() {
        return httpResponseString.getBody();
    }

    public int getLastHttpResponseStatus() {
        return httpResponseString.getStatus();
    }

    public void setHttpClient(HttpClient httpClient) {
        Unirest.setHttpClient(httpClient);
    }

    private UtilBean utilBean() {
        return BddPicoContainer.instance().getComponent(UtilBean.class);
    }

    public enum RequestFields {
        METHOD,
        URI,
        BODY,
        BODY_FILE,
        HEADER,
        DESCRIPTION
    }
}
