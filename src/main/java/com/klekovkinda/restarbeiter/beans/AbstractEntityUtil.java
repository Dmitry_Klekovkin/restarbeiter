package com.klekovkinda.restarbeiter.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractEntityUtil<T> {

    public abstract void doIndexFields(String id, T value);
    public abstract void doIndexField(String id, String value);

    public void doIndexFieldsInList(String id, List<T> values) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(id + ".size()", values.size());
        addIndexes(properties);
        for (T value : values) {
            if(value instanceof Map || value instanceof List){
                doIndexFields(id + ".get(" + values.indexOf(value) + ")", value);
            }else{
                doIndexField(id + ".get(" + values.indexOf(value) + ")", value.toString());
            }
        }
    }

    protected void addIndexes(Map<String, Object> map) {
        for (String key : map.keySet()) {
            System.out.println(key + "\t" + map.get(key));
        }
        map().putAll(map);
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }
}
