package com.klekovkinda.restarbeiter.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilBean {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public String replace(String sourceString) throws JsonProcessingException {
        if (sourceString == null) {
            return "";
        }
        Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}");
        Matcher matcher = pattern.matcher(sourceString);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String id = matcher.group(1);
            Object value = map().get(id);
            if (!(value instanceof String)) {
                value = objectMapper.writeValueAsString(value);
            }
            matcher.appendReplacement(sb, value.toString());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public Map<HTTPClientBean.RequestFields, String> convertGherkinRowsToRequestMap(DataTable dataTable) {
        Map<HTTPClientBean.RequestFields, String> valuesMap = new HashMap<>();
        if (dataTable.getGherkinRows().size() == 2) {
            List<String> headersList = dataTable.getGherkinRows().get(0).getCells();
            List<String> valuesList = dataTable.getGherkinRows().get(1).getCells();
            for (int i = 0; i < headersList.size(); i++) {
                HTTPClientBean.RequestFields requestField = HTTPClientBean.RequestFields.valueOf(headersList.get(i));
                if (requestField == null) {
                    continue;
                }
                valuesMap.put(requestField, valuesList.get(i));
            }
        }
        return valuesMap;
    }

    private HashMap map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }
}
