package com.klekovkinda.restarbeiter.beans;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class ConstantsBean {
    public static final String DOMAIN = "DOMAIN";

    public ConstantsBean() throws UnknownHostException {
        map().putAll(System.getenv());
        map().put(DOMAIN, InetAddress.getLocalHost().getCanonicalHostName());
    }

    private Map map() {
        return BddPicoContainer.instance().getComponent(HashMap.class);
    }
}
