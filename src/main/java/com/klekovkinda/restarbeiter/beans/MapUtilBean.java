package com.klekovkinda.restarbeiter.beans;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapUtilBean extends AbstractEntityUtil<Map> {
    @Override
    public void doIndexFields(String id, Map value) {
        Map<String, Object> indexMap = new LinkedHashMap<>();
        indexMap.put(id, value);
        Set<String> fields = value.keySet();
        for (String field : fields) {
            Object fieldValue = value.get(field);
            indexMap.put(id + "." + field, fieldValue);
            if (fieldValue instanceof Map) {
                doIndexFields(id + "." + field, (Map) fieldValue);
            }
            if (fieldValue instanceof List) {
                doIndexFieldsInList(id + "." + field, (List) fieldValue);
            }
        }
        addIndexes(indexMap);
    }

    @Override
    public void doIndexField(String id, String value) {
        Map<String, Object> indexMap = new LinkedHashMap<>();
        indexMap.put(id, value);
        addIndexes(indexMap);
    }
}
